#pragma once
#include "GameState.h"

class Render 
{
private:
	GameState* const m_gameState;
	sf::RenderWindow m_gameWindow;
public:
	void renderFrame();
	void initWindow();
	Render(GameState*);
};