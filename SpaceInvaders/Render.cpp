#include "Render.h"
#include <iostream>
void Render::renderFrame()
{
	auto player = m_gameState->getPlayer();
    this->m_gameWindow.draw(player.getRect());
    this->m_gameWindow.display();

	return;
}

void Render::initWindow()
{
    sf::RenderWindow window(sf::VideoMode(200, 200), "SFML works!");
    sf::Event event;
    while (window.pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
            window.close();
    }
    window.clear();
    window.display();
}

Render::Render(GameState* const gameState) : m_gameState(gameState)
{

}

