#pragma once
#include <vector>
#include <chrono>
#include "Entity.h"
#include "Projectile.h"
#include "Player.h"
#include "SpaceShip.h"

class GameState
{
public:
	GameState();
	const Player& getPlayer() {return *m_player;};
private:
	std::vector<SpaceShip>				m_enemyShips;
	std::vector<Projectile>				m_activeProjectiles;
	long long							m_lastProccesing;
	Player*								m_player;
};
