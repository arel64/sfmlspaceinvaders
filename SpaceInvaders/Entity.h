#pragma once
#include <SFML\Graphics\RectangleShape.hpp>
class Entity
{
protected:
	sf::RectangleShape*	m_rect;
	int					m_collisionBehaviour;
	double				m_xVelocity;
	double				m_yVelocity;
	double				m_xAcceleration;
	double				m_yAcceleration;
	Entity(sf::RectangleShape* rect,int collisionBehaviour, double xVelocity, double yVelocity, double xAcceleration, double yAcceleration) :
		m_rect(rect), m_collisionBehaviour(collisionBehaviour), m_xVelocity(xVelocity), m_yVelocity(yVelocity), m_xAcceleration(xAcceleration), m_yAcceleration(yAcceleration) {};
};