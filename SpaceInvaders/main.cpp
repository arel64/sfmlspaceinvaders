#include <SFML/Graphics.hpp>
#include <chrono>
#include "GameState.h"
#include "Game.h"
#include "Render.h"
#include <iostream>
//working
constexpr double MS_PER_TICK = 1000 / 60;
auto getCurrentTimeMs() {
    return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}
int main()
{
    // Use separate high resolution clock for calculating milliseconds.
    auto proccesedTime = getCurrentTimeMs();
    std::cout << proccesedTime << std::endl;
    GameState *gameState = new GameState();
    Render *renderer = new Render(gameState);
    Game *game = new Game(gameState);
    game->startGame(*renderer);
    while (game->isGameRunning()) {
        /*
        Render Frame
        */
        renderer->renderFrame();
        while (proccesedTime + MS_PER_TICK < getCurrentTimeMs()) {
            game->update();
            proccesedTime += MS_PER_TICK;
        }
    }
    

    return 0;
}
