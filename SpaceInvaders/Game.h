#pragma once
#include "GameState.h"
#include "Render.h"
class Game
{
public:
	Game(GameState* gameState) : m_gameState(gameState), m_isGameRunning(false) {};
	int		isGameRunning() { return m_isGameRunning; };
	void	update();
	void	startGame(Render& gameRenderer);
private:
	GameState*	m_gameState;
	bool		m_isGameRunning;
};