#pragma once
#include "Entity.h"
class Projectile : public Entity
{
protected:
	bool	m_isFriendly;
	double	m_damage;
	Projectile(bool isFriendly, double damage) : Entity(m_rect,m_collisionBehaviour, m_xVelocity, m_yVelocity, m_xAcceleration, m_yAcceleration), m_isFriendly(isFriendly),m_damage(damage){}
};