#pragma once
#include "Entity.h"
#include <SFML/Graphics.hpp>
class Player : public Entity {
public:
	Player(int shotsPerTick, double health,sf::RectangleShape* m_rect,int m_collisionBehaviour,double m_xVelocitym,double m_yVelocity,double m_xAcceleration,double m_yAcceleration) : 
		Entity(m_rect, m_collisionBehaviour, m_xVelocity, m_yVelocity, m_xAcceleration, m_yAcceleration), m_shotsPerTick(shotsPerTick), m_health(health) {}
	Player(sf::RectangleShape* m_rect, int m_collisionBehaviour, double m_xVelocitym, double m_yVelocity, double m_xAcceleration, double m_yAcceleration) : 
		Entity(m_rect, m_collisionBehaviour, m_xVelocity, m_yVelocity, m_xAcceleration, m_yAcceleration),m_shotsPerTick(1),m_health(100){}
	const sf::RectangleShape& getRect() { return *m_rect; };
private:
	unsigned	m_shotsPerTick;
	double		m_health;
};